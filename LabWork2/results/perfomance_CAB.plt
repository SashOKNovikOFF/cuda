reset

set terminal png size 1024, 768
set output 'Perfomance_CAB.png'

set xrange [0:6000]

set xlabel "N (размерность матриц)"
set ylabel "Производительность, ГФлопс"

set logscale y 2

plot "PERFOMANCE_CAB" using 1:2 with points notitle, \
     "PERFOMANCE_CAB" using 1:3 with points notitle, \
     "PERFOMANCE_CAB" using 1:4 with points notitle, \
     "PERFOMANCE_CAB" using 1:5 with points notitle, \
     "PERFOMANCE_CAB" using 1:2 smooth csplines lw 3 ti "Последовательная, CPU", \
     "PERFOMANCE_CAB" using 1:3 smooth csplines lw 3 ti "Параллельная, CPU", \
     "PERFOMANCE_CAB" using 1:4 smooth csplines lw 3 ti "Глобальная память, GPU", \
     "PERFOMANCE_CAB" using 1:5 smooth csplines lw 3 ti "Разделяемая память, GPU"