reset

set terminal png size 1024, 768
set output 'Perfomance_AAT.png'

set xlabel "N (размерность матриц)"
set ylabel "Производительность, ГФлопс"

#set logscale y 2

plot "PERFOMANCE_AAT" using 1:2 with points notitle, \
     "PERFOMANCE_AAT" using 1:3 with points notitle, \
     "PERFOMANCE_AAT" using 1:4 with points notitle, \
     "PERFOMANCE_AAT" using 1:2 with lines lw 3 ti "Вариант А", \
     "PERFOMANCE_AAT" using 1:3 with lines lw 3 ti "Вариант Б", \
     "PERFOMANCE_AAT" using 1:4 with lines lw 3 ti "Вариант Б*"