reset

set terminal png size 1024, 768
set output 'Speedup_CAB.png'

set xrange [0:6000]

set xlabel "N (размерность матриц)"
set ylabel "Ускорение"

set logscale y 2

plot "SPEEDUP_CAB" using 1:2 with points notitle, \
     "SPEEDUP_CAB" using 1:3 with points notitle, \
     "SPEEDUP_CAB" using 1:4 with points notitle, \
     "SPEEDUP_CAB" using 1:2 smooth csplines lw 3 ti "Параллельная, CPU", \
     "SPEEDUP_CAB" using 1:3 smooth csplines lw 3 ti "Глобальная память, GPU", \
     "SPEEDUP_CAB" using 1:4 smooth csplines lw 3 ti "Разделяемая память, GPU"