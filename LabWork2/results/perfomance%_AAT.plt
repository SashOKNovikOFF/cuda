reset

set terminal png size 1024, 768
set output 'Perfomance%_AAT.png'

set xlabel "N (размерность матриц)"
set ylabel "Производительность в процентах от пиковой, %"

plot "PERFOMANCE%_AAT" using 1:2 with points notitle, \
     "PERFOMANCE%_AAT" using 1:3 with points notitle, \
     "PERFOMANCE%_AAT" using 1:4 with points notitle, \
     "PERFOMANCE%_AAT" using 1:2 with lines lw 3 ti "Вариант А", \
     "PERFOMANCE%_AAT" using 1:3 with lines lw 3 ti "Вариант Б", \
     "PERFOMANCE%_AAT" using 1:4 with lines lw 3 ti "Вариант Б*"