import subprocess

time = []

dataT = 0.0
calcT = 0.0
genT  = 0.0
N = 10
for x in range(0, N):
	pipe = subprocess.Popen(['./LabWork2', '4096'], stdout=subprocess.PIPE)
	text = pipe.communicate()[0]
	dataT += float(text.split()[0])
	calcT += float(text.split()[1])
	genT  += float(text.split()[2])

print("Time of data changing: %f") % (dataT / N)
print("Time of calculating work: %f") % (calcT / N)
print("Time of program work: %f") % (genT  / N)
