// Стандартные заголовки C
#include <stdio.h>  // Для ввода/вывода данных
#include <math.h>   // Для математических функций
#include <time.h>   // Для функции time()
#include <stdlib.h> // Для malloc()/calloc()/free()

// Разное
const int sizeBlock = 32;
const bool DEBUG = false;

__global__ void multiplyGlobal(float* matrixA, float* matrixB, float* matrixC, int length);
__global__ void multiplyShared(float* matrixA, float* matrixB, float* matrixC, int length);

bool checkInput(int& matrixLength, int& argc, char** argv); // Проверка ввода данных

void initRandomMatrix(float* matrix, int length); // Инициализация матрицы случайными числами
void initUserMatrix(float* matrix, int length);   // Инициализация матрицы заданной программистом
void initZeroMatrix(float* matrix, int length);   // Инициализация матрицы нулями

void printMatrix(float* matrix, int length);      // Вывод матрицы на экран
float getMatrixNorm(float* matrix, int length);   // Вычисление нормы матрицы

int main(int argc, char** argv)
{
	int matrixLength;
	
	//!< Проверка ввода данных длины матрицы
	bool flag = checkInput(matrixLength, argc, argv);
	if (flag != 0) return flag;
	
	// Данные по количеству потоков, блоков
	int numThreads = matrixLength;
	dim3 threads(sizeBlock, sizeBlock);
	dim3 blocks(numThreads / sizeBlock, numThreads / sizeBlock);
	
	//!< Матрицы A, B, C для CPU и GPU
	float* matrixA; float* nvMatrixA;
	float* matrixB; float* nvMatrixB;
	float* matrixC; float* nvMatrixC;
		
	//CUDA: Выделение памяти на создание трёх матриц
	cudaMalloc((void **)&nvMatrixA, matrixLength * matrixLength * sizeof(float));
	cudaMalloc((void **)&nvMatrixB, matrixLength * matrixLength * sizeof(float));
	cudaMalloc((void **)&nvMatrixC, matrixLength * matrixLength * sizeof(float));
	
	//!< Выделение памяти на создание трёх матриц
	matrixA = (float*)malloc(matrixLength * matrixLength * sizeof(float));
	matrixB = (float*)malloc(matrixLength * matrixLength * sizeof(float));
	matrixC = (float*)malloc(matrixLength * matrixLength * sizeof(float));
	
	//!< Заполнение матриц
	srand((unsigned int)time(NULL));
	initRandomMatrix(matrixA, matrixLength);
	initRandomMatrix(matrixB, matrixLength);
	
	//CUDA: Ввод событий для замера времени работы программы
	cudaEvent_t start, stop;
	float genTime, dataTime, calcTime, time;
	genTime = dataTime = calcTime = time = 0.0;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	
	//CUDA: Замеряем время обмена данными HostToDevice
	cudaEventRecord(start, 0);
	
	//CUDA: Копирование данных на GPU
	cudaMemcpy(nvMatrixA, matrixA, matrixLength * matrixLength * sizeof(float), cudaMemcpyDefault);
	cudaMemcpy(nvMatrixB, matrixB, matrixLength * matrixLength * sizeof(float), cudaMemcpyDefault);
	
	//CUDA: Заканчиваем замер времени обмена данными HostToDevice
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&time, start, stop);
	dataTime += time;
	genTime  += time;
	
	//CUDA: Замеряем время расчёта
	cudaEventRecord(start, 0);
	
	//CUDA: Запуск ядра программы
	//multiplyGlobal<<<blocks, threads>>>(nvMatrixA, nvMatrixB, nvMatrixC, matrixLength);
	multiplyShared<<<blocks, threads>>>(nvMatrixA, nvMatrixB, nvMatrixC, matrixLength);
	
	//CUDA: Заканчиваем замер времени расчёта
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&time, start, stop);
	calcTime += time;
	genTime += time;
	
	//CUDA: Замеряем время обмена данными DeviceToHost
	cudaEventRecord(start, 0);
	
	//CUDA: Получить результат расчётов
	cudaMemcpy(matrixC, nvMatrixC, matrixLength * matrixLength * sizeof(float), cudaMemcpyDefault);

	//CUDA: Заканчиваем замер времени обмена данными DeviceToHost
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&time, start, stop);
	dataTime += time;
	genTime  += time;

	//!< Вывод результатов на экран
	//printf("Time of data changing: %f\n", dataTime);
	//printf("Time of calculating work: %f\n", calcTime);
	//printf("Time of program work: %f\n", genTime);
	//printf("Matrix norm: %f\n", getMatrixNorm(matrixC, matrixLength));
	printf("%f %f %f\n", dataTime, calcTime, genTime);
	
	//DEBUG: Вывод результирующей матрицы на экран
	if (DEBUG) printMatrix(matrixC, matrixLength);
	
	//!< Очистка памяти
	free(matrixA);
	free(matrixB);
	free(matrixC);
	
	//CUDA: Очистка памяти
	cudaFree(nvMatrixA);
	cudaFree(nvMatrixB);
	cudaFree(nvMatrixC);
	
	//CUDA: Удаляем события
	cudaEventDestroy(start);
	cudaEventDestroy(stop);
	
    return 0;
}

__global__ void multiplyGlobal(float* matrixA, float* matrixB, float* matrixC, int length)
{
	int row    = blockIdx.y * blockDim.y + threadIdx.y;
	int column = blockIdx.x * blockDim.x + threadIdx.x;	
	
	float sum = 0.0f;
	for (int i = 0; i < length; i++)
		sum += matrixA[row * length + i] * matrixB[i * length + column];
	matrixC[row * length + column] = sum;
}
__global__ void multiplyShared(float *matrixA, float *matrixB, float *matrixC, int length)
{
	int bx = blockIdx.x;  int by = blockIdx.y;
	int tx = threadIdx.x; int ty = threadIdx.y;
	int ix = bx * blockDim.x + tx;
	int iy = by * blockDim.y + ty;
	
	float sum = 0.0f;

	__shared__ float aShared[sizeBlock][sizeBlock];
	__shared__ float bShared[sizeBlock][sizeBlock];

	for(int i = 0; i < length / sizeBlock; i++)
	{
		aShared[ty][tx] = matrixA[(by * sizeBlock + ty) * length + i * sizeBlock + tx];
		bShared[ty][tx] = matrixB[(i * sizeBlock + ty) * length + bx * sizeBlock + tx];
		
		__syncthreads();
		
		for(int j = 0; j < sizeBlock; j++)
			sum += aShared[ty][j] * bShared[j][tx];
		
		__syncthreads();
	};

	matrixC[iy * length + ix] = sum;
}

bool checkInput(int& matrixLength, int& argc, char** argv)
{
	if (argc != 2)
	{
		printf("Incorrect argument numbers.\n");
		return -1;
	}
	else
	{
		matrixLength = atoi(argv[1]);
		
		if (matrixLength == 0)
		{
			printf("Incorrect matrix length.\n");
			return -2;
		}
		else if (matrixLength % 32 != 0)
		{
			printf("Matrix length must be divisible by 32.\n");
			return -3;
		};
	};
	
	return 0;
}

void initRandomMatrix(float* matrix, int length)
{
	for (int i = 0; i < length; i++)
		for (int j = 0; j < length; j++)
			matrix[i * length + j] = (float)rand() / RAND_MAX - 0.5f;
}
void initUserMatrix(float* matrix, int length)
{
	for (int i = 0; i < length; i++)
		for (int j = 0; j < length; j++)
			matrix[i * length + j] = i + j;
}
void initZeroMatrix(float* matrix, int length)
{
	for (int i = 0; i < length; i++)
		for (int j = 0; j < length; j++)
			matrix[i * length + j] = 0.0f;
}

void printMatrix(float* matrix, int length)
{
	for (int i = 0; i < length; i++)
	{
		for (int j = 0; j < length; j++)
			printf("%f\t", matrix[i * length + j]);
		printf("\n");
	};
}
float getMatrixNorm(float* matrix, int length)
{
	float sum = 0;
	for (int i = 0; i < length; i++)
		for (int j = 0; j < length; j++)
			sum += matrix[i * length + j] * matrix[i * length + j];
	
	return sqrt(sum);
}
