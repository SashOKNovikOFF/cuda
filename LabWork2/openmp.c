// Стандартные заголовки C
#include <stdio.h>  // Для ввода/вывода данных
#include <math.h>   // Для математических функций
#include <time.h>   // Для функции time()
#include <stdlib.h> // Для malloc()/calloc()/free()
#include <omp.h>    // Для распараллеливания на CPU

int checkInput(int* matrixLength, int argc, char** argv); // Проверка ввода данных

void allocateMemory(float*** matrix, int length);   // Выделение памяти под квадратную матрицу
void deallocateMemory(float*** matrix, int length); // Очищение памяти под квадратную матрицу

void initRandomMatrix(float** matrix, int length);  // Инициализация матрицы случайными числами
void initUserMatrix(float** matrix, int length);    // Инициализация матрицы заданной программистом

void printMatrix(float** matrix, int length);       // Вывод матрицы на экран
float getMatrixNorm(float** matrix, int length);   // Вычисление нормы матрицы
int main(int argc, char** argv)
{
	int length = 256;
	
	//!< Проверка ввода данных длины матрицы
	//int flag = checkInput(matrixLength, argc, argv);
	//if (flag != 0) return flag;
	
	float** matrixA;
	float** matrixB;
	float** matrixC;
    
	//!< Выделение памяти на создание трёх матриц
	allocateMemory(&matrixA, length);
	allocateMemory(&matrixB, length);
	allocateMemory(&matrixC, length);

	//!< Заполнение матриц случайными числами
	srand((unsigned int)time(NULL));
	initRandomMatrix(matrixA, length);
	initRandomMatrix(matrixB, length);

	float beginTime;
	float endTime;
	float resultTime;

	//!< Включаем таймер (время расчёта)
	beginTime = omp_get_wtime();

	//!< Произведение двух матриц
	#pragma omp parallel for if (length >= 20)
	for (int i = 0; i < length; i++)
	{
		for (int j = 0; j < length; j++)
		{
			for (int k = 0; k < length; k++)
			{
				matrixC[i][j] += matrixA[i][k] * matrixB[k][j];
			};
		};
	};
	
	//!< Выключаем таймер (время расчёта)
	endTime = omp_get_wtime();
	resultTime = endTime - beginTime;

	//!< Вывод результатов на экран
	printf("%lf\n", resultTime);

	//!< Очистка памяти
	deallocateMemory(&matrixA, length);
	deallocateMemory(&matrixB, length);
	deallocateMemory(&matrixC, length);
    
    return 0;
}

int checkInput(int* matrixLength, int argc, char** argv)
{
	if (argc != 2)
	{
		printf("Incorrect argument numbers.\n");
		return -1;
	}
	else
	{
		*matrixLength = atoi(argv[1]);
		
		if (*matrixLength == 0)
		{
			printf("Incorrect matrix length.\n");
			return -2;
		}
		else if (*matrixLength % 32 != 0)
		{
			printf("Matrix length must be divisible by 32.\n");
			return -3;
		};
	};
	
	return 0;
}

void allocateMemory(float*** matrix, int length)
{
	*matrix = (float**)malloc(length * sizeof(float*));
	for (int i = 0; i < length; i++)
		(*matrix)[i] = (float*)calloc(length, sizeof(float));
};
void deallocateMemory(float*** matrix, int length)
{
	for (int i = 0; i < length; i++)
		free((*matrix)[i]);
	free(*matrix);
};

void initRandomMatrix(float** matrix, int length)
{
	for (int i = 0; i < length; i++)
		for (int j = 0; j < length; j++)
			matrix[i][j] = (float)rand() / RAND_MAX - 0.5;
};

void printMatrix(float** matrix, int length)
{
	for (int i = 0; i < length; i++)
	{
		for (int j = 0; j < length; j++)
			printf("%f\t", matrix[i][j]);
		printf("\n");
	};
};
void initUserMatrix(float** matrix, int length)
{
	for (int i = 0; i < length; i++)
		for (int j = 0; j < length; j++)
			matrix[i][j] = i + j;
};

float getMatrixNorm(float** matrix, int length)
{
	float sum = 0;
	for (int i = 0; i < length; i++)
		for (int j = 0; j < length; j++)
			sum += matrix[i][j] * matrix[i][j];
	
	return sqrt(sum);
};
