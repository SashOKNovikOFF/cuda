reset

set terminal png size 1024, 768
set output 'Time_4096.png'

set xlabel "Число потоков"
set ylabel "Время выполнения цикла, мс"

set xrange [0:32]

plot "4096.txt" using 1:2 with lines lw 4 notitle
#     "PERFOMANCE%_AAT" using 1:3 with points notitle, \
#     "PERFOMANCE%_AAT" using 1:4 with points notitle, \
#     "PERFOMANCE%_AAT" using 1:2 with lines lw 3 ti "Вариант А", \
#     "PERFOMANCE%_AAT" using 1:3 with lines lw 3 ti "Вариант Б", \
#     "PERFOMANCE%_AAT" using 1:4 with lines lw 3 ti "Вариант Б*"