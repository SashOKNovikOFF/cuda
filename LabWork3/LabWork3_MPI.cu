// Стандартные заголовки C
#include "mpi.h"    // Для использования технологии MPI
#include <stdio.h>  // Для ввода/вывода данных
#include <math.h>   // Для математических функций
#include <time.h>   // Для функции time()
#include <stdlib.h> // Для malloc()/calloc()/free()

// Разное
const int sizeBlock = 32;
const bool DEBUG = false;

__global__ void multiplyShared(float* matrixA, float* matrixB, float* matrixC, int length);

int checkInput(int& matrixLength, int& argc, char** argv); // Проверка ввода данных

void initRandomMatrix(float* matrix, int length); // Инициализация матрицы случайными числами
void initUserMatrix(float* matrix, int length);   // Инициализация матрицы заданной программистом
void initZeroMatrix(float* matrix, int length);   // Инициализация матрицы нулями

void printMatrix(float* matrix, int length);      // Вывод матрицы на экран
float getMatrixNorm(float* matrix, int length);   // Вычисление нормы матрицы

int main(int argc, char** argv)
{
	//! MPI: Инициализация
    MPI_Init(&argc, &argv);
    
    int processID;    // ID процесса
    int processCount; // Число процессов
    int cardNumber;   // Число видеокарт
    
    //! MPI: Определение числа процессов и ID процесса
	MPI_Comm_size(MPI_COMM_WORLD, &processCount);
	MPI_Comm_rank(MPI_COMM_WORLD, &processID);
    
    //CUDA: Определение числа видеокарт
	cudaGetDeviceCount(&cardNumber);  
	cudaSetDevice(processID % cardNumber);
	
	int matrixLength; // Размерность матриц
	
	//!< Проверка ввода данных длины матрицы (выполняют все процессы!)
	int flag = checkInput(matrixLength, argc, argv);
	if (flag != 0)
	{
		MPI_Finalize();
		return flag;
	};
	
	// Данные по количеству потоков, блоков
	int numThreads = matrixLength;
	dim3 threads(sizeBlock, sizeBlock);
	dim3 blocks(numThreads / sizeBlock, numThreads / (sizeBlock * processCount));
	
	//!< Матрицы A, B, C для CPU и GPU
	float* matrixA; float* nvMatrixA;
	float* matrixB; float* nvMatrixB;
	float* matrixC; float* nvMatrixC;
		
	//CUDA: Выделение памяти на создание трёх матриц
	cudaMalloc((void **)&nvMatrixA, matrixLength * matrixLength / processCount * sizeof(float));
	cudaMalloc((void **)&nvMatrixB, matrixLength * matrixLength * sizeof(float));
	cudaMalloc((void **)&nvMatrixC, matrixLength * matrixLength / processCount * sizeof(float));
	
	//CUDA: Выделение памяти на создание матрицы B (pinned-память)
	cudaMallocHost((void **)&matrixB, matrixLength * matrixLength * sizeof(float));
	
	//! MPI: Если нулевой процесс
	if (processID == 0)
	{
		//CUDA: Выделяем pinned-память на создание матриц A и C
		cudaMallocHost((void **)&matrixA, matrixLength * matrixLength * sizeof(float));
		cudaMallocHost((void **)&matrixC, matrixLength * matrixLength * sizeof(float));
		
		//!< Заполнение матриц
		srand((unsigned int)time(NULL));
		if (DEBUG)
		{
			initUserMatrix(matrixA, matrixLength);
			initUserMatrix(matrixB, matrixLength);
		}
		else
		{
			initRandomMatrix(matrixA, matrixLength);
			initRandomMatrix(matrixB, matrixLength);
		};
	};
	
	//CUDA: Ввод событий для замера времени работы программы
	cudaEvent_t start, stop;
	float genTime, H_D_Time, calcTime, D_H_Time, time;
	genTime = H_D_Time = D_H_Time = calcTime = time = 0.0;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	
	//CUDA: Замеряем время передачи данных HostToDevice
	cudaEventRecord(start, 0);
	
	//! MPI: Разделяем матрицу A и передаём "куски" всем процессам
	MPI_Scatter(matrixA, matrixLength * matrixLength / processCount, MPI_FLOAT, nvMatrixA, matrixLength * matrixLength / processCount, MPI_FLOAT, 0, MPI_COMM_WORLD);
	
	//! MPI: Передаём матрицу B всем процессам и копируем её на GPU
	MPI_Bcast(matrixB, matrixLength * matrixLength, MPI_FLOAT, 0, MPI_COMM_WORLD);
	cudaMemcpy(nvMatrixB, matrixB, matrixLength * matrixLength * sizeof(float), cudaMemcpyDefault);
	
	//CUDA: Заканчиваем замер времени передачи данных HostToDevice
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&time, start, stop);
	H_D_Time += time;
	genTime  += time;
	
	//CUDA: Замеряем время расчёта
	cudaEventRecord(start, 0);
	
	//CUDA: Запуск ядра программы
	multiplyShared<<<blocks, threads>>>(nvMatrixA, nvMatrixB, nvMatrixC, matrixLength);
	
	//CUDA: Заканчиваем замер времени расчёта
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&time, start, stop);
	calcTime += time;
	genTime += time;
	
	//CUDA: Замеряем время обмена данными DeviceToHost
	cudaEventRecord(start, 0);
	
	//! MPI: Собираем "куски" матрицы C с GPU и передаём нулевому процессу
	MPI_Gather(nvMatrixC, matrixLength * matrixLength / processCount, MPI_FLOAT, matrixC, matrixLength * matrixLength / processCount, MPI_FLOAT, 0, MPI_COMM_WORLD);
	
	//CUDA: Заканчиваем замер времени обмена данными DeviceToHost
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&time, start, stop);
	D_H_Time += time;
	genTime  += time;

	//! MPI: Если нулевой процесс
	if (processID == 0)
	{
		//!< Вывод результатов на экран
		//printf("Time of data changing HtoD: %f\n", H_D_Time);
		//printf("Time of calculating work: %f\n", calcTime);
		//printf("Time of data changing DtoH: %f\n", D_H_Time);
		//printf("Time of program work: %f\n", genTime);
		//printf("Matrix norm: %f\n", getMatrixNorm(matrixC, matrixLength));
		printf("%f %f %f %f\n", H_D_Time, calcTime, D_H_Time, genTime);
		
		
		//DEBUG: Вывод результирующей матрицы на экран
		if (DEBUG) printMatrix(matrixC, matrixLength);
		
		//!< Очистка памяти (pinned-память)
		cudaFreeHost(matrixA);
		cudaFreeHost(matrixC);
	};

	//!< Очистка памяти (pinned-память)
	cudaFreeHost(matrixB);
	
	//CUDA: Очистка памяти
	cudaFree(nvMatrixA);
	cudaFree(nvMatrixB);
	cudaFree(nvMatrixC);
	
	//CUDA: Удаляем события
	cudaEventDestroy(start);
	cudaEventDestroy(stop);
	
	MPI_Finalize();
	
    return 0;
}

__global__ void multiplyShared(float *matrixA, float *matrixB, float *matrixC, int length)
{
	int bx = blockIdx.x;  int by = blockIdx.y;
	int tx = threadIdx.x; int ty = threadIdx.y;
	int ix = bx * blockDim.x + tx;
	int iy = by * blockDim.y + ty;
	
	float sum = 0.0f;

	__shared__ float aShared[sizeBlock][sizeBlock];
	__shared__ float bShared[sizeBlock][sizeBlock];

	for(int i = 0; i < length / sizeBlock; i++)
	{
		aShared[ty][tx] = matrixA[(by * sizeBlock + ty) * length + i * sizeBlock + tx];
		bShared[ty][tx] = matrixB[(i * sizeBlock + ty) * length + bx * sizeBlock + tx];
		
		__syncthreads();
		
		for(int j = 0; j < sizeBlock; j++)
			sum += aShared[ty][j] * bShared[j][tx];
		
		__syncthreads();
	};

	matrixC[iy * length + ix] = sum;
}

int checkInput(int& matrixLength, int& argc, char** argv)
{
	if (argc != 2)
	{
		printf("Incorrect argument numbers.\n");
		return -1;
	}
	else
	{
		matrixLength = atoi(argv[1]);
		
		if (matrixLength == 0)
		{
			printf("Incorrect matrix length.\n");
			return -2;
		}
		else if (matrixLength % 32 != 0)
		{
			printf("Matrix length must be divisible by 32.\n");
			return -3;
		};
	};
	
	return 0;
}

void initRandomMatrix(float* matrix, int length)
{
	for (int i = 0; i < length; i++)
		for (int j = 0; j < length; j++)
			matrix[i * length + j] = (float)rand() / RAND_MAX - 0.5f;
}
void initUserMatrix(float* matrix, int length)
{
	for (int i = 0; i < length; i++)
		for (int j = 0; j < length; j++)
			matrix[i * length + j] = i + j;
}
void initZeroMatrix(float* matrix, int length)
{
	for (int i = 0; i < length; i++)
		for (int j = 0; j < length; j++)
			matrix[i * length + j] = 0.0f;
}

void printMatrix(float* matrix, int length)
{
	for (int i = 0; i < length; i++)
	{
		for (int j = 0; j < length; j++)
			printf("%f\t", matrix[i * length + j]);
		printf("\n");
	};
}
float getMatrixNorm(float* matrix, int length)
{
	float sum = 0;
	for (int i = 0; i < length; i++)
		for (int j = 0; j < length; j++)
			sum += matrix[i * length + j] * matrix[i * length + j];
	
	return sqrt(sum);
}
