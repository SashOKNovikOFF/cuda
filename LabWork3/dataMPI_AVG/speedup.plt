reset

set terminal png size 1024, 768
set output 'Speedup_KernelTime.png'

set xrange [0:4100]

set xlabel "N (размерность матриц)"
set ylabel "Ускорение относительно времени выполнения ядра"

plot "Speedup.txt" using 1:3 with lines notitle