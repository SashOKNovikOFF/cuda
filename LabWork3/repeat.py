import subprocess

time = []

H_D_T = 0.0
calcT = 0.0
D_H_T = 0.0
genT  = 0.0
N = 3
for x in range(0, N):
	pipe = subprocess.Popen(['mpirun', '-n', '2', './a.out', '512'], stdout=subprocess.PIPE)
	text = pipe.communicate()[0]
	H_D_T += float(text.split()[0])
	calcT += float(text.split()[1])
	D_H_T += float(text.split()[2])
	genT  += float(text.split()[3])

print("Time of data changing HtoD: %f") % (H_D_T / N)
print("Time of calculating work: %f") % (calcT / N)
print("Time of data changing DtoH: %f") % (D_H_T / N)
print("Time of program work: %f") % (genT  / N)
