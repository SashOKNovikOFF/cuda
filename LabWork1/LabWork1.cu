#include <stdio.h>

const int NUMTHR_BLOCK = 1024;
const int MAX_BLOCKS = 1024;
const long NUMTHR_ALL   = (NUMTHR_BLOCK - 24) * MAX_BLOCKS;

// 0.02 ms, 0.19 ms, 1.80 ms, 18.18 ms, 26.42 ms

__global__ void add(int *a, int *b, int *c) {
	long index = threadIdx.x + blockIdx.x * blockDim.x;
	if (index < NUMTHR_ALL) c[index] = a[index] + b[index];
}

void initialize_data(int *a, const int number, const long N) {
	for (uint i = 0; i < N; i++)
		a[i] = number;
}

void check_results(int *a, int *b, int *c, const long N) {
	bool flag = true;
	for (long i = 0; i < N; i++)
		if (c[i] != (a[i] + b[i])) {

			flag = false;
			break;
		}

	printf("Results (1 - correct, 0 - incorrect): %d\n", flag);
}

int main(void) {
	int *a, *b, *c;
	int *d_a, *d_b, *d_c;
	long size = NUMTHR_ALL * sizeof(int);

	cudaMalloc((void **)&d_a, size);
	cudaMalloc((void **)&d_b, size);
	cudaMalloc((void **)&d_c, size);

	a = (int *)malloc(size); initialize_data(a, 0, NUMTHR_ALL);
	b = (int *)malloc(size); initialize_data(b, 1, NUMTHR_ALL);
	c = (int *)malloc(size);

	cudaMemcpy(d_a, a, size, cudaMemcpyDefault);
	cudaMemcpy(d_b, b, size, cudaMemcpyDefault);

	cudaEvent_t start, stop;
	float time = 0.0;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	cudaEventRecord(start, 0);

	add<<<(NUMTHR_ALL + NUMTHR_BLOCK - 1) / NUMTHR_BLOCK, NUMTHR_BLOCK>>>(d_a, d_b, d_c);
	cudaDeviceSynchronize();

	cudaError_t error = cudaGetLastError();
	if (error != cudaSuccess) {
		printf("CUDA error: %s\n", cudaGetErrorString(error));
		return 0;
	};

	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&time, start, stop);	

	cudaMemcpy(c, d_c, size, cudaMemcpyDefault);
	check_results(a, b, c, NUMTHR_ALL);
	printf("Elapsed time : %.2f ms\n", time);

	// Очистить выделенную память

	free(a);
	free(b);
	free(c);

	cudaFree(d_a);
	cudaFree(d_b);
	cudaFree(d_c);

	cudaEventDestroy(start);
	cudaEventDestroy(stop);

	return 0;
}
